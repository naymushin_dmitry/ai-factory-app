package dev.dthompson.photoeditor;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.transition.Fade;
import android.transition.TransitionInflater;
import android.transition.TransitionSet;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import dev.dthompson.photoeditor.configuration.Configuration;
import dev.dthompson.photoeditor.fragment.CameraFragment;
import dev.dthompson.photoeditor.fragment.FilterFragment;
import dev.dthompson.photoeditor.fragment.SplashFragment;
import dev.dthompson.photoeditor.fragment.StartFragment;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements StartFragment.OnClickListener {
    private FragmentManager fragmentManager;

    public static final int PIX_REQUEST_CODE = 123;

    private final static String KEY_DISPOSED = "key_disposed";

    private Disposable disposable;
    private boolean disposed;

    static {
        System.loadLibrary("NativeImageProcessor");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fragmentManager = getSupportFragmentManager();

        if (savedInstanceState != null) {
            disposed = savedInstanceState.getBoolean(KEY_DISPOSED, false);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (disposable != null) {
            outState.putBoolean(KEY_DISPOSED, disposable.isDisposed());
        } else {
            outState.putBoolean(KEY_DISPOSED, disposed);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (disposable == null && !disposed) {
            showSplashScreen();
            disposable = Flowable.timer(Configuration.SPLASH_SCREEN_DURATION_SECONDS, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> transitionToStartScreen());
        }
    }

    public void showSplashScreen() {
        SplashFragment splashFragment = SplashFragment.newInstance();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, splashFragment)
                .commit();
    }

    private void transitionToStartScreen() {
        final long TRANSITION_MOVE_TIME = 1000;
        final long TRANSITION_FADE_TIME = 300;

        if (isDestroyed()) {
            return;
        }

        Fragment splashFrament = fragmentManager.findFragmentById(R.id.fragment_container);

        Fragment startFragment = StartFragment.newInstance();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        // 1. Exit for SplashFragment
        Fade exitFade = new Fade();
        exitFade.setDuration(TRANSITION_FADE_TIME);
        splashFrament.setExitTransition(exitFade);

        // 2. Shared Elements Transition
        TransitionSet enterTransitionSet = new TransitionSet();
        enterTransitionSet.addTransition(TransitionInflater.from(this)
                .inflateTransition(android.R.transition.move));
        enterTransitionSet.setDuration(TRANSITION_MOVE_TIME);
        enterTransitionSet.setStartDelay(TRANSITION_FADE_TIME);
        startFragment.setSharedElementEnterTransition(enterTransitionSet);

        // 3. Enter Transition for StartFragment
        Fade enterFade = new Fade();
        enterFade.setStartDelay(TRANSITION_MOVE_TIME + TRANSITION_FADE_TIME);
        enterFade.setDuration(TRANSITION_FADE_TIME);
        startFragment.setEnterTransition(enterFade);

        View logo = findViewById(R.id.ivLogo);
        fragmentTransaction.addSharedElement(logo, logo.getTransitionName());
        fragmentTransaction.replace(R.id.fragment_container, startFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_CANCELED) {
            onBackPressed();
        } else if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PIX_REQUEST_CODE) {
                String resultImagePath = (data.getStringArrayListExtra(Pix.IMAGE_RESULTS)).get(0);
                transitionToFilterScreen(resultImagePath);
            }
        }
    }

    private void transitionToFilterScreen(String imagePath) {
        FilterFragment filterFragment = FilterFragment.newInstance(imagePath);

        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                        R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.fragment_container, filterFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions,
                                           @NotNull int[] grantResults) {
        if (requestCode == PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                onBackPressed();
                Toast.makeText(
                        this,
                        getString(R.string.permissions_required),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick() {
        transitionToCameraScreen();
    }

    private void transitionToCameraScreen() {
        CameraFragment cameraFragment = CameraFragment.newInstance();

        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                        R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.fragment_container, cameraFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}