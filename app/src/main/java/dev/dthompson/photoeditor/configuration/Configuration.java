package dev.dthompson.photoeditor.configuration;

public final class Configuration {
    public final static String DIRECTORY = "/photoeditor";

    public final static int SPLASH_SCREEN_DURATION_SECONDS = 2;

    public final static int MAXIMUM_PHOTOS_AT_A_TIME = 1;

    public static final String IMAGE_FILE_EXTENSION = "jpeg";
}