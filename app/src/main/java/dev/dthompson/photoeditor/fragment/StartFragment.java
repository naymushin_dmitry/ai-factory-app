package dev.dthompson.photoeditor.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.dthompson.photoeditor.R;

public class StartFragment extends Fragment {
    public interface OnClickListener {
        void onClick();
    }

    private OnClickListener onClickListener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        onClickListener = (OnClickListener) context;
    }

    @BindView(R.id.tvGuide)
    TextView tvGuide;

    @BindView(R.id.fabStart)
    FloatingActionButton fabStart;

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);

        ButterKnife.bind(this, view);

        setAnimation(tvGuide);

        fabStart.setOnClickListener(v -> onClickListener.onClick());

        return view;
    }

    private void setAnimation(TextView textView) {
        Animation fadeIn = new AlphaAnimation(0.2f, 1.0f);
        fadeIn.setDuration(750);
        fadeIn.setStartOffset(0);

        Animation fadeOut = new AlphaAnimation(1.0f, 0.2f);
        fadeOut.setDuration(750);
        fadeOut.setStartOffset(0);

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                textView.startAnimation(fadeOut);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                textView.startAnimation(fadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
            }

            @Override
            public void onAnimationStart(Animation arg0) {
            }
        });

        textView.startAnimation(fadeOut);
    }
}