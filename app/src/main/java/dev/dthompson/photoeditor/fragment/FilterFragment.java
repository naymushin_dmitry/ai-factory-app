package dev.dthompson.photoeditor.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.zomato.photofilters.SampleFilters;
import com.zomato.photofilters.imageprocessors.Filter;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.dthompson.photoeditor.R;
import dev.dthompson.photoeditor.thumbnail.ThumbnailCallback;
import dev.dthompson.photoeditor.thumbnail.ThumbnailItem;
import dev.dthompson.photoeditor.thumbnail.ThumbnailsAdapter;
import dev.dthompson.photoeditor.thumbnail.ThumbnailsManager;
import dev.dthompson.photoeditor.utils.BitmapUtils;

import static dev.dthompson.photoeditor.configuration.Configuration.IMAGE_FILE_EXTENSION;

public class FilterFragment extends Fragment implements ThumbnailCallback {
    private static final String KEY_IMAGE_PATH = "key_image_path";
    private static final String KEY_SELECTED_FILTER_POSITION = "key_selected_filter_position";

    private List<ThumbnailItem> thumbnailItems;

    private int selectedFilterPosition;

    @BindView(R.id.ivPlaceHolder)
    ImageView ivPlaceHolder;

    @BindView(R.id.rvThumbnails)
    RecyclerView rvThumbnails;

    @BindView(R.id.fabCancel)
    FloatingActionButton fabCancel;

    @BindView(R.id.fabShare)
    FloatingActionButton fabShare;

    public static FilterFragment newInstance(String imagePath) {
        FilterFragment filterFragment = new FilterFragment();

        Bundle args = new Bundle();
        args.putString(KEY_IMAGE_PATH, imagePath);
        filterFragment.setArguments(args);

        return filterFragment;
    }

    private String getImagePath() {
        return getArguments().getString(KEY_IMAGE_PATH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);

        ButterKnife.bind(this, view);

        if (savedInstanceState != null) {
            selectedFilterPosition = savedInstanceState.getInt(KEY_SELECTED_FILTER_POSITION, 0);
        }

        fabCancel.setOnClickListener(v -> getActivity().onBackPressed());
        fabShare.setOnClickListener(v -> prepareAndShareImage());

        initFilterList();
        initUI();

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_SELECTED_FILTER_POSITION, selectedFilterPosition);
    }

    private void initFilterList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        } else {
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        }

        layoutManager.scrollToPosition(selectedFilterPosition);

        rvThumbnails.setLayoutManager(layoutManager);
        rvThumbnails.setHasFixedSize(true);

        bindDataToAdapter();
    }

    private void bindDataToAdapter() {
        Bitmap thumbImage = BitmapUtils.readBitmapFromImageFile(getImagePath());

        ThumbnailItem item1 = new ThumbnailItem();
        ThumbnailItem item2 = new ThumbnailItem();
        ThumbnailItem item3 = new ThumbnailItem();
        ThumbnailItem item4 = new ThumbnailItem();
        ThumbnailItem item5 = new ThumbnailItem();
        ThumbnailItem item6 = new ThumbnailItem();

        item1.image = thumbImage;
        item2.image = thumbImage;
        item3.image = thumbImage;
        item4.image = thumbImage;
        item5.image = thumbImage;
        item6.image = thumbImage;

        item1.title = getString(R.string.filter_none);
        item2.title = getString(R.string.filter_star_lit);
        item3.title = getString(R.string.filter_blue_mess);
        item4.title = getString(R.string.filter_awe_struck_vibe);
        item5.title = getString(R.string.filter_lime_stutter);
        item6.title = getString(R.string.filter_night_whisper);

        ThumbnailsManager.clearThumbs();
        ThumbnailsManager.addThumb(item1); // original image

        item2.filter = SampleFilters.getStarLitFilter();
        ThumbnailsManager.addThumb(item2);

        item3.filter = SampleFilters.getBlueMessFilter();
        ThumbnailsManager.addThumb(item3);

        item4.filter = SampleFilters.getAweStruckVibeFilter();
        ThumbnailsManager.addThumb(item4);

        item5.filter = SampleFilters.getLimeStutterFilter();
        ThumbnailsManager.addThumb(item5);

        item6.filter = SampleFilters.getNightWhisperFilter();
        ThumbnailsManager.addThumb(item6);

        thumbnailItems = ThumbnailsManager.processThumbs(getContext());

        thumbnailItems.get(selectedFilterPosition).selected = true;

        ThumbnailsAdapter adapter = new ThumbnailsAdapter(thumbnailItems, this,
                getResources(), selectedFilterPosition);
        rvThumbnails.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void initUI() {
        Filter lastFilter = thumbnailItems.get(selectedFilterPosition).filter;

        Bitmap bitmap = applyFilterToOriginalImage(lastFilter);
        ivPlaceHolder.setImageBitmap(bitmap);
    }

    @Override
    public void onThumbnailClick(int position, Filter filter) {
        selectedFilterPosition = position;

        Bitmap editedBitmap = applyFilterToOriginalImage(filter);
        ivPlaceHolder.setImageBitmap(editedBitmap);
    }

    private void prepareAndShareImage() {
        Filter filter = thumbnailItems.get(selectedFilterPosition).filter;

        Bitmap editedBitmap = applyFilterToOriginalImage(filter);
        String imagePath = createPathForNewImageFile(getImagePath());

        boolean result = BitmapUtils.createImageFileFromBitmap(imagePath, editedBitmap);

        if (result) {
            Uri imageUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                imageUri = FileProvider.getUriForFile(getContext(),
                        getContext().getApplicationContext().getPackageName() + ".provider",
                        new File(imagePath));
            } else {
                imageUri = Uri.parse(getString(R.string.uri_scheme_file) + imagePath);
            }

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/" + IMAGE_FILE_EXTENSION);
            shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);

            startActivity(Intent.createChooser(shareIntent, getString(R.string.share_title)));
        } else {
            Toast.makeText(getContext(), getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap applyFilterToOriginalImage(Filter filter) {
        Bitmap originalBitmap = BitmapUtils.readBitmapFromImageFile(getImagePath());
        return filter.processFilter(originalBitmap.copy(originalBitmap.getConfig(), true));
    }

    private String createPathForNewImageFile(String originalImageFilePath) {
        int indexOfDot = originalImageFilePath.lastIndexOf(getString(R.string.dot));
        String imageFileNameBare = originalImageFilePath.substring(0, indexOfDot);
        return imageFileNameBare + getString(R.string.underscore)
                + System.currentTimeMillis() + getString(R.string.dot) + IMAGE_FILE_EXTENSION;
    }
}