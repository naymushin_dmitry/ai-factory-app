package dev.dthompson.photoeditor.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;

import java.util.concurrent.TimeUnit;

import dev.dthompson.photoeditor.MainActivity;
import dev.dthompson.photoeditor.R;
import dev.dthompson.photoeditor.configuration.Configuration;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CameraFragment extends Fragment {
    private Disposable disposable;

    private MainActivity activity;

    public static CameraFragment newInstance() {
        return new CameraFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (disposable == null || disposable.isDisposed()) {
            disposable = Flowable.timer(500, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> initPix());
        }
    }

    private void initPix() {
        Options options = Options.init()
                .setRequestCode(MainActivity.PIX_REQUEST_CODE)
                .setCount(Configuration.MAXIMUM_PHOTOS_AT_A_TIME)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.HIGH)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_FULL_USER)
                .setPath(Configuration.DIRECTORY);
        Pix.start(activity, options);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (disposable != null) {
            disposable.dispose();
        }
    }
}