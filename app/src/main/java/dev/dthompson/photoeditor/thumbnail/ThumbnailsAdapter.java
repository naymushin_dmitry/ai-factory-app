package dev.dthompson.photoeditor.thumbnail;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import dev.dthompson.photoeditor.R;

public class ThumbnailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ThumbnailCallback thumbnailCallback;

    private List<ThumbnailItem> dataSet;

    private Resources resources;

    private int lastPosition;

    public ThumbnailsAdapter(List<ThumbnailItem> dataSet, ThumbnailCallback thumbnailCallback,
                             Resources resources, int lastPosition) {
        this.dataSet = dataSet;
        this.thumbnailCallback = thumbnailCallback;
        this.resources = resources;
        this.lastPosition = lastPosition;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.thumbnail_item, viewGroup, false);
        return new ThumbnailsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {
        ThumbnailItem thumbnailItem = dataSet.get(position);

        ThumbnailsViewHolder thumbnailsViewHolder = (ThumbnailsViewHolder) holder;

        thumbnailsViewHolder.tvTitle.setText(thumbnailItem.title);

        boolean selected = thumbnailItem.selected;
        thumbnailsViewHolder.tvTitle.setTextColor(
                resources.getColor(selected ? R.color.orange_800 : R.color.white));

        thumbnailsViewHolder.ivOutline.setVisibility(selected ? View.VISIBLE : View.GONE);

        thumbnailsViewHolder.ivThumbnail.setImageBitmap(thumbnailItem.image);
        thumbnailsViewHolder.ivThumbnail.setOnClickListener(v -> {
            if (lastPosition != position) {
                thumbnailCallback.onThumbnailClick(position, thumbnailItem.filter);

                dataSet.get(lastPosition).selected = false;
                dataSet.get(position).selected = true;
                notifyDataSetChanged();

                lastPosition = position;
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ThumbnailsViewHolder extends RecyclerView.ViewHolder {
        ImageView ivThumbnail;
        ImageView ivOutline;
        TextView tvTitle;

        ThumbnailsViewHolder(View v) {
            super(v);
            ivThumbnail = v.findViewById(R.id.ivThumbnail);
            ivOutline = v.findViewById(R.id.ivOutline);
            tvTitle = v.findViewById(R.id.tvTitle);
        }
    }
}