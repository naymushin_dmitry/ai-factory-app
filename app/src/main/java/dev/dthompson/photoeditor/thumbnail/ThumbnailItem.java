package dev.dthompson.photoeditor.thumbnail;

import android.graphics.Bitmap;

import com.zomato.photofilters.imageprocessors.Filter;

public class ThumbnailItem {
    public Bitmap image;
    public Filter filter;
    public boolean selected;
    public String title;

    public ThumbnailItem() {
        filter = new Filter();
    }
}