package dev.dthompson.photoeditor.thumbnail;

import com.zomato.photofilters.imageprocessors.Filter;

public interface ThumbnailCallback {
    void onThumbnailClick(int position, Filter filter);
}